#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <vitasdk.h>

#include "ctrl.h"
#include "debugScreen.h"

#define printf psvDebugScreenPrintf
#define clearScreen psvDebugScreenClear

int main(){
  psvDebugScreenInit();
  clearScreen(0);
  scePowerRequestColdReset();
  return 0;
}


